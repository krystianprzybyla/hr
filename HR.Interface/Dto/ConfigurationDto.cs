﻿using HR.Interface.Enums;

namespace HR.Interface.Dto
{
    public class ConfigurationDto
    {
        public ConfigurationDto(ContractType contractType, int experienceFrom, int experienceTo, decimal minimalSalary)
        {
            ContractType = contractType;
            ExperienceFrom = experienceFrom;
            ExperienceTo = experienceTo;
            MinimalSalary = minimalSalary;
        }

        public ContractType ContractType { get; set; }
        public int ExperienceFrom { get; set; }
        public int ExperienceTo { get; set; }
        public decimal MinimalSalary { get; set; }
    }
}

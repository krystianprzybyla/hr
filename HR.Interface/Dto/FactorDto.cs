﻿using HR.Interface.Enums;

namespace HR.Interface.Dto
{
    public class FactorDto
    {
        public FactorDto(ContractType contractType, decimal factorA, decimal factorB)
        {
            ContractType = contractType;
            FactorA = factorA;
            FactorB = factorB;
        }

        public ContractType ContractType { get; set; }
        public decimal FactorA { get; set; }
        public decimal FactorB { get; set; }
    }
}

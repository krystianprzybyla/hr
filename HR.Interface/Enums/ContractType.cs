﻿using System.ComponentModel.DataAnnotations;

namespace HR.Interface.Enums
{
    public enum ContractType
    {
        [Display(Name = "Programmer")]
        Programmer = 0,
        [Display(Name = "Tester")]
        Tester = 1,
    }
}

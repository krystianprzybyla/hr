﻿using System.Collections.Generic;
using HR.Interface.Enums;
using HR.Interface.Dto;

namespace HR.Interface
{
    public interface IContractCalculator
    {
        decimal CalculateSalary(ContractType contractType, int experience);
        void LoadConfiguration(IList<ConfigurationDto> configurations, IList<FactorDto> factors);
    }
}

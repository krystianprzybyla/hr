﻿using HR.Interface;
using HR.Interface.Dto;
using HR.Interface.Enums;
using System.Collections.Generic;
using System.Linq;

namespace HR.Business.Logic
{
    public class ContractCalculator : IContractCalculator
    {
        private IList<ConfigurationDto> _configurations;
        private IList<FactorDto> _factors;

        #region IContractCalculator

        public void LoadConfiguration(IList<ConfigurationDto> configurations, IList<FactorDto> factors)
        {
            _configurations = configurations;
            _factors = factors;
        }

        public decimal CalculateSalary(ContractType contractType, int experience)
        {
            var minimalSalary = GetMinimalSalary(contractType, experience);

            var factor = _factors.Single(f => f.ContractType == contractType);

            return minimalSalary
                + experience * factor.FactorA
                + minimalSalary * factor.FactorB;
        }

        #endregion IContractCalculator

        #region private

        private decimal GetMinimalSalary(ContractType contractType, int experience)
        {
            var configuration = _configurations
                .Where(c =>
                    c.ContractType == contractType
                    && experience >= c.ExperienceFrom
                    && experience <= c.ExperienceTo)
                .OrderBy(c =>
                    c.ExperienceTo - c.ExperienceFrom)
                .FirstOrDefault();

            return configuration?.MinimalSalary ?? 0;
        }

        #endregion private
    }
}
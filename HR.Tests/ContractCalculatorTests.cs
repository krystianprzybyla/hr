﻿using HR.Business.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using HR.Interface;
using HR.Interface.Dto;
using HR.Interface.Enums;

namespace HR.Tests
{
    [TestClass]
    public class ContractCalculatorTests
    {
        private IContractCalculator _calculator;

        [TestInitialize]
        public void Initialize()
        {
            var configurations = new List<ConfigurationDto>()
            {
                new ConfigurationDto(ContractType.Programmer, 1, 1, 2500),
                new ConfigurationDto(ContractType.Programmer, 1, 3, 2500),
                new ConfigurationDto(ContractType.Programmer, 3, 3, 5000),
                new ConfigurationDto(ContractType.Programmer, 3, 5, 5000),
                new ConfigurationDto(ContractType.Programmer, 5, 5, 5000),
                new ConfigurationDto(ContractType.Programmer, 5, 100, 5500),
                new ConfigurationDto(ContractType.Tester, 1, 1, 2000),
                new ConfigurationDto(ContractType.Tester, 1, 2, 2000),
                new ConfigurationDto(ContractType.Tester, 2, 2, 2700),
                new ConfigurationDto(ContractType.Tester, 2, 4, 2700),
                new ConfigurationDto(ContractType.Tester, 4, 4, 2700),
                new ConfigurationDto(ContractType.Tester, 4, 100, 3200),
            };

            var factors = new List<FactorDto>()
            {
                new FactorDto(ContractType.Programmer, 125, 0),
                new FactorDto(ContractType.Tester, 100, 0.25m),
            };

            _calculator = new ContractCalculator();
            _calculator.LoadConfiguration(configurations, factors);
        }

        #region Programmer

        [TestMethod]
        public void ProgrammerSalary0()
        {
            var result = _calculator.CalculateSalary(ContractType.Programmer, 0);
            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void ProgrammerSalary01()
        {
            var result = _calculator.CalculateSalary(ContractType.Programmer, 1);
            Assert.AreEqual(result, 2625);
        }

        [TestMethod]
        public void ProgrammerSalary04()
        {
            var result = _calculator.CalculateSalary(ContractType.Programmer, 4);
            Assert.AreEqual(result, 5500);
        }

        [TestMethod]
        public void ProgrammerSalary05()
        {
            var result = _calculator.CalculateSalary(ContractType.Programmer, 5);
            Assert.AreEqual(result, 5625);
        }

        [TestMethod]
        public void ProgrammerSalary10()
        {
            var result = _calculator.CalculateSalary(ContractType.Programmer, 10);
            Assert.AreEqual(result, 6750);
        }

        #endregion Programmer

        #region Tester

        [TestMethod]
        public void TesterSalary01()
        {
            var result = _calculator.CalculateSalary(ContractType.Tester, 1);
            Assert.AreEqual(result, 2600);
        }

        [TestMethod]
        public void TesterSalary02()
        {
            var result = _calculator.CalculateSalary(ContractType.Tester, 2);
            Assert.AreEqual(result, 3575);
        }

        [TestMethod]
        public void TesterSalary03()
        {
            var result = _calculator.CalculateSalary(ContractType.Tester, 3);
            Assert.AreEqual(result, 3675);
        }

        [TestMethod]
        public void TesterSalary10()
        {
            var result = _calculator.CalculateSalary(ContractType.Tester, 10);
            Assert.AreEqual(result, 5000);
        }

        #endregion Tester
    }
}

﻿using HR.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HR.Business.Interface
{
    public interface IContractCalculator
    {
        decimal CalculateSalary(ContractType type, int experience);
        decimal GetMinimalSalary(ContractType type, int experience);
    }
}

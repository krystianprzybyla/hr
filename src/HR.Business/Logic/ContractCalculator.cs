﻿using HR.Business.Interface;
using HR.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HR.Business.Logic
{
    public class ContractCalculator : IContractCalculator
    {
        #region IContractCalculator

        public decimal CalculateSalary(ContractType type, int experience)
        {
            decimal ret = default(decimal);

            var minimalSalary = GetMinimalSalary(type, experience);

            switch (type)
            {
                case ContractType.Programmer:
                    ret = CalculateSalaryForProgrammer(experience, minimalSalary);
                    break;
                case ContractType.Tester:
                    ret = CalculateSalaryForTester(experience, minimalSalary);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return ret;
        }

        public decimal GetMinimalSalary(ContractType type, int experience)
        {
            var ret = default(decimal);

            switch (type)
            {
                case ContractType.Programmer:
                    ret = GetMinimalSalaryForProgrammer(experience);
                    break;
                case ContractType.Tester:
                    ret = GetMinimalSalaryForTester(experience);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return ret;
        }

        #endregion IContractCalculator

        #region private

        private decimal GetMinimalSalaryForProgrammer(int experience)
        {
            var ret = 0;

            if (experience >= 1 && experience < 3)
            {
                ret = 2500;
            }
            else if (experience >= 3 && experience <= 5)
            {
                ret = 5000;
            }
            else if (experience > 5)
            {
                ret = 5500;
            }

            return ret;
        }

        private decimal GetMinimalSalaryForTester(int experience)
        {
            var ret = 0;

            if (experience >= 1 && experience < 2)
            {
                ret = 2000;
            }
            else if (experience >= 2 && experience <= 4)
            {
                ret = 2700;
            }
            else if (experience > 4)
            {
                ret = 3200;
            }

            return ret;
        }

        private decimal CalculateSalaryForProgrammer(int experience, decimal minimalSalary)
        {
            return minimalSalary + (experience * 125);
        }

        private decimal CalculateSalaryForTester(int experience, decimal minimalSalary)
        {
            return minimalSalary + (experience * 100 + (minimalSalary / 4));
        }

        #endregion private
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HR.Tests
{
    [TestClass]
    public class ContractCalculatorTests
    {
        [DataTestMethod]
        [DataRow(ContractType.Programmer, 1, 2500)]
        public void EvaluateMinimalSalary(ContractType type, int experience, decimal expectedValue)
        {
            var calculator = new ContractCalculator();
            var result = calculator.GetMinimalSalary(type, experience + 1);
            Assert.AreEqual(result, expectedValue);
        }
    }
}

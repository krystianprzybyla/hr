using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HR.Data;
using HR.Models;
using HR.Models.ContractViewModels;
using HR.Interface.Enums;
using HR.Interface;
using HR.Interface.Dto;
using HR.Services;

namespace HR.Controllers
{
    public class ContractsController : Controller
    {
        public const int EXPENSIVE_CONTRACT_LIMIT = 5000;
        public const int EXPERIENCED_PROGRAMMER_LIMIT = 5;
        public const int PAGE_SIZE = 10;

        private readonly ApplicationDbContext _context;
        private readonly IContractCalculator _contractCalculator;

        public ContractsController(ApplicationDbContext context, ContractCalculatorService contractCalculator)
        {
            _context = context;
            _contractCalculator = contractCalculator;
        }

        // GET: Contracts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Contracts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ContractType,Experience,Name")] Contract contract)
        {
            _appendSalary(contract);

            if (ModelState.IsValid)
            {
                _context.Add(contract);
                await _context.SaveChangesAsync();
                return RedirectToAction("Filter");
            }

            return View(contract);
        }

        // GET: Contracts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract.SingleOrDefaultAsync(m => m.Id == id);

            _appendSalary(contract);

            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // POST: Contracts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ContractType,Experience,Name")] Contract contract)
        {
            if (id != contract.Id)
            {
                return NotFound();
            }

            _appendSalary(contract);

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contract);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContractExists(contract.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Filter");
            }

            return View(contract);
        }

        // GET: Contracts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = await _context.Contract.SingleOrDefaultAsync(m => m.Id == id);

            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        // POST: Contracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contract = await _context.Contract.SingleOrDefaultAsync(m => m.Id == id);
            _context.Contract.Remove(contract);
            await _context.SaveChangesAsync();
            return RedirectToAction("Filter");
        }

        private bool ContractExists(int id)
        {
            return _context.Contract.Any(e => e.Id == id);
        }

        // GET: Contracts/Evaluate/
        public IActionResult Evaluate(int contractType, int experience)
        {
            var salary = _contractCalculator.CalculateSalary((ContractType)contractType, experience);

            return Json(new { salary = salary });
        }

        // GET: Contracts/Filter/
        [HttpGet]
        public IActionResult Filter()
        {
            var contracts = _context.Contract.OrderBy(o => o.Name).AsQueryable();
            var viewModel = new FilterViewModel();
            contracts = _paginate(contracts, viewModel);
            viewModel.Contracts = contracts.ToList();

            return View(viewModel);
        }

        // POST: Contracts/Filter/
        [HttpPost]
        public IActionResult Filter(FilterViewModel viewModel)
        {
            var contracts = _appendFilters(viewModel, _context.Contract.OrderBy(o => o.Name));
            contracts = _paginate(contracts, viewModel);
            viewModel.Contracts = contracts.ToList();

            return View(viewModel);
        }

        #region private

        private IQueryable<Contract> _paginate(IQueryable<Contract> contract, FilterViewModel viewModel)
        {
            viewModel.PageCount = (int)Math.Ceiling(contract.Count() / (decimal)PAGE_SIZE);
            viewModel.Page = Math.Min(viewModel.Page, viewModel.PageCount);

            var skip = (viewModel.Page - 1) * PAGE_SIZE;

            return contract
                .Skip(skip)
                .Take(PAGE_SIZE);
        }

        private void _appendSalary(Contract contract)
        {

            contract.Salary = _contractCalculator.CalculateSalary(contract.ContractType, contract.Experience);
        }

        private static IQueryable<Contract> _appendFilters(FilterViewModel viewModel, IQueryable<HR.Models.Contract> contracts)
        {
            var ret = contracts;

            ret = _appendNameFilter(viewModel, contracts);
            ret = _appendContractTypeFilter(viewModel, contracts, ret);
            ret = _appendExperienceFilters(viewModel, contracts, ret);
            ret = _appendSalaryFilters(viewModel, contracts, ret);
            ret = _appendAdditionalFilters(viewModel, ret);

            return ret;
        }

        private static IQueryable<Contract> _appendNameFilter(FilterViewModel viewModel, IQueryable<Contract> contracts)
        {
            var ret = contracts;

            if (viewModel.FilterName != null)
            {
                ret = contracts.Where(c => c.Name.IndexOf(viewModel.FilterName, StringComparison.InvariantCultureIgnoreCase) >= 0);
            }

            return ret;
        }

        private static IQueryable<Contract> _appendContractTypeFilter(FilterViewModel viewModel, IQueryable<Contract> contracts, IQueryable<Contract> ret)
        {
            if (viewModel.FilterContractType != null)
            {
                ret = contracts.Where(c => c.ContractType == viewModel.FilterContractType.Value);
            }

            return ret;
        }

        private static IQueryable<Contract> _appendExperienceFilters(FilterViewModel viewModel, IQueryable<Contract> contracts, IQueryable<Contract> ret)
        {
            if (viewModel.FilterExperienceMin > 0)
            {
                ret = contracts.Where(c => c.Experience >= viewModel.FilterExperienceMin);
            }

            if (viewModel.FilterExperienceMax > 0)
            {
                ret.Where(c => c.Experience <= viewModel.FilterExperienceMin);
            }

            return ret;
        }

        private static IQueryable<Contract> _appendSalaryFilters(FilterViewModel viewModel, IQueryable<Contract> contracts, IQueryable<Contract> ret)
        {
            if (viewModel.FilterSalaryMin > 0)
            {
                ret = contracts.Where(c => c.Salary >= viewModel.FilterSalaryMin);
            }

            if (viewModel.FilterSalaryMax > 0)
            {
                ret = ret.Where(c => c.Salary <= viewModel.FilterSalaryMin);
            }

            return ret;
        }

        private static IQueryable<Contract> _appendAdditionalFilters(FilterViewModel viewModel, IQueryable<Contract> ret)
        {
            if (viewModel.FilterExpensiveContracts)
            {
                ret = ret.Where(c => c.Salary > EXPENSIVE_CONTRACT_LIMIT);
            }

            if (viewModel.FilterExperiencedProgrammers)
            {
                ret = ret.Where(c =>
                    c.Experience >= EXPERIENCED_PROGRAMMER_LIMIT
                    && c.ContractType == ContractType.Programmer);
            }

            return ret;
        }

        #endregion private
    }
}

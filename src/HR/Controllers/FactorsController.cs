using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HR.Data;
using HR.Models;

namespace HR.Controllers
{
    public class FactorsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FactorsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Factors
        public async Task<IActionResult> Index()
        {
            return View(await _context.Factor.ToListAsync());
        }

        // GET: Factors/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var factor = await _context.Factor.SingleOrDefaultAsync(m => m.Id == id);
            if (factor == null)
            {
                return NotFound();
            }
            return View(factor);
        }

        // POST: Factors/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ContractType,FactorA,FactorB")] Factor factor)
        {
            if (id != factor.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(factor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FactorExists(factor.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(factor);
        }

        private bool FactorExists(int id)
        {
            return _context.Factor.Any(e => e.Id == id);
        }
    }
}

﻿class PaginationHelper {
    static GoToPage(element, page)
    {
        var form = $(element).parents('form');
        var pageField = form.find('#Page');
        $(pageField).val(page);
        form.submit();
        return false;
    }
}
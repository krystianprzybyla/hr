﻿'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var SalaryHelper = (function () {
    function SalaryHelper() {
        _classCallCheck(this, SalaryHelper);
    }

    _createClass(SalaryHelper, null, [{
        key: 'RecalculateSalary',
        value: function RecalculateSalary(element) {
            var _this = this;

            var form = this.GetForm(element);
            var experience = $(form).find('#Experience').val();
            var contractType = $(form).find('#ContractType').val();
            var evaluateUrl = "/Contracts/Evaluate?contractType=" + contractType + "&experience=" + experience;

            $.get(evaluateUrl, function (data) {
                _this.SetSalary(form, data.salary);
            });
        }
    }, {
        key: 'GetForm',
        value: function GetForm(element) {
            return $(element).parents('form').first();
        }
    }, {
        key: 'SetSalary',
        value: function SetSalary(form, value) {
            $(form).find('#Salary').val(value);
        }
    }]);

    return SalaryHelper;
})();


﻿class SalaryHelper {
    static RecalculateSalary(element)
    {
        var form = this.GetForm(element);
        var experience = $(form).find('#Experience').val();
        var contractType = $(form).find('#ContractType').val();
        var evaluateUrl = "/Contracts/Evaluate?contractType=" + contractType + "&experience=" + experience;

        $.get(evaluateUrl, (data) => { this.SetSalary(form, data.salary); });
    }

    static GetForm(element)
    {
        return $(element).parents('form').first();
    }

    static SetSalary(form, value)
    {
        $(form).find('#Salary').val(value);
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HR.Data.Migrations
{
    public partial class Configuration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Configuration",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractType = table.Column<int>(nullable: false),
                    ExperienceFrom = table.Column<int>(nullable: false),
                    ExperienceTo = table.Column<int>(nullable: false),
                    MinimalSalary = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Configuration", x => x.Id);
                });

            migrationBuilder.Sql(@"
                SET IDENTITY_INSERT Configuration ON;

                INSERT INTO Configuration
                ([Id], [ContractType], [ExperienceFrom], [ExperienceTo], [MinimalSalary])
                VALUES
                (1,  0, 1, 1,   2500),
                (2,  0, 1, 3,   2500),
                (3,  0, 3, 3,   5000),
                (4,  0, 3, 5,   5000),
                (5,  0, 5, 5,   5000),
                (6,  0, 5, 100, 5500),
                (7,  1, 1, 1,   2000),
                (8,  1, 1, 2,   2000),
                (9,  1, 2, 2,   2700),
                (10, 1, 2, 4,   2700),
                (11, 1, 4, 4,   2700),
                (12, 1, 4, 100, 3200);

                SET IDENTITY_INSERT Configuration OFF;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Configuration");
        }
    }
}

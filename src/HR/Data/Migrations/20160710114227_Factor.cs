﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HR.Data.Migrations
{
    public partial class Factor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Factor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContractType = table.Column<int>(nullable: false),
                    FactorA = table.Column<decimal>(nullable: false),
                    FactorB = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Factor", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Factor_ContractType",
                table: "Factor",
                column: "ContractType",
                unique: true);


            migrationBuilder.Sql(@"
                SET IDENTITY_INSERT Factor ON;

                INSERT INTO Factor
                ([Id], [ContractType], [FactorA], [FactorB])
                VALUES
                (1,  0, 125, 0),
                (2,  1, 100, 0.25);

                SET IDENTITY_INSERT Factor OFF;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Factor");
        }
    }
}

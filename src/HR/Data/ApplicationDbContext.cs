﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using HR.Models;

namespace HR.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Factor>()
                .HasIndex(i => i.ContractType)
                .IsUnique(true);
        }

        public DbSet<Contract> Contract { get; set; }
        public DbSet<Configuration> Configuration { get; set; }
        public DbSet<Factor> Factor { get; set; }
    }
}

﻿using HR.Business.Logic;
using HR.Data;
using HR.Interface.Dto;
using System.Collections.Generic;

namespace HR.Services
{
    public class ContractCalculatorService : ContractCalculator
    {
        private ApplicationDbContext _context;

        public ContractCalculatorService(ApplicationDbContext context)
        {
            _context = context;

            var configurationDtoList = new List<ConfigurationDto>();
            var factorDtoList = new List<FactorDto>();

            foreach (var configuration in _context.Configuration)
            {
                configurationDtoList.Add(new ConfigurationDto(
                        contractType: configuration.ContractType,
                        experienceFrom: configuration.ExperienceFrom,
                        experienceTo: configuration.ExperienceTo,
                        minimalSalary: configuration.MinimalSalary
                    ));
            }

            foreach (var factor in _context.Factor)
            {
                factorDtoList.Add(new FactorDto(
                        contractType: factor.ContractType,
                        factorA: factor.FactorA,
                        factorB: factor.FactorB
                    ));
            }

            LoadConfiguration(configurationDtoList, factorDtoList);
        }
    }
}

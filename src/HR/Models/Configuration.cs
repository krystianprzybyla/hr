﻿using HR.Interface.Enums;
using System.ComponentModel.DataAnnotations;

namespace HR.Models
{
    public class Configuration
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Type")]
        [EnumDataType(typeof(ContractType), ErrorMessage = "Unknown value, please check.")]
        public ContractType ContractType { get; set; }

        [Required]
        [Display(Name = "Experience from")]
        [Range(0, 100, ErrorMessage = "Please enter value from 0 to 100")]
        public int ExperienceFrom { get; set; }

        [Required]
        [Display(Name = "Experience to")]
        [Range(0, 100, ErrorMessage = "Please enter value from 0 to 100")]
        public int ExperienceTo { get; set; }

        [Required]
        [Display(Name = "Minimal salary")]
        public decimal MinimalSalary { get; set; }
    }
}

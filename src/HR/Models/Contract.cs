﻿using HR.Interface.Enums;
using System.ComponentModel.DataAnnotations;

namespace HR.Models
{
    public class Contract
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Type")]
        [EnumDataType(typeof(ContractType), ErrorMessage = "Unknown value, please check.")]
        public ContractType ContractType { get; set; }

        [Required]
        [Display(Name = "Experience")]
        [Range(0, 100, ErrorMessage = "Please enter value from 0 to 100")]
        public int Experience { get; set; }

        [Required]
        [Display(Name = "Salary")]
        public decimal Salary { get; set; }
    }
}

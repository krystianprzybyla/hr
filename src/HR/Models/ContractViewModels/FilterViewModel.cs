﻿using HR.Interface.Enums;
using HR.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HR.Models.ContractViewModels
{
    public class FilterViewModel
    {
        public FilterViewModel()
        {
            Page = 1;
        }

        public int Id { get; set; }

        #region Contract

        public List<Contract> Contracts { get; set; }

        #endregion Contract

        #region Filter

        [Display(Name = "Expensive contracts")]
        public bool FilterExpensiveContracts { get; set; }

        [Display(Name = "Experienced programmers")]
        public bool FilterExperiencedProgrammers { get; set; }

        [Display(Name = "Name")]
        public string FilterName { get; set; }

        [Display(Name = "Contract Type")]
        public ContractType? FilterContractType { get; set; }

        [Display(Name = "Experience Min")]
        [Range(0, 100, ErrorMessage = "{0} is expected to be between {1} and {2}")]
        public int FilterExperienceMin { get; set; }

        [Display(Name = "Experience Max")]
        [Range(0, 100, ErrorMessage = "{0} is expected to be between {1} and {2}")]
        public int FilterExperienceMax { get; set; }

        [Display(Name = "Salary Min")]
        [Range(0, 100000, ErrorMessage = "{0} is expected to be between {1} and {2}")]
        public decimal FilterSalaryMin { get; set; }

        [Display(Name = "Salary Max")]
        [Range(0, 100000, ErrorMessage = "{0} is expected to be between {1} and {2}")]
        public decimal FilterSalaryMax { get; set; }

        #endregion Filter

        #region Pagination

        public int Page { get; set; }
        public int PageCount { get; set; }

        #endregion Pagination
    }
}

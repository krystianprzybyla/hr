﻿using HR.Interface.Enums;
using System.ComponentModel.DataAnnotations;

namespace HR.Models
{
    public class Factor
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Type")]
        [EnumDataType(typeof(ContractType), ErrorMessage = "Unknown value, please check.")]
        public ContractType ContractType { get; set; }

        [Required]
        [Display(Name = "Factor A")]
        public decimal FactorA { get; set; }

        [Required]
        [Display(Name = "Factor B")]
        public decimal FactorB { get; set; }
    }
}
